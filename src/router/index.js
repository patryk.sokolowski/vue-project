import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Home from '@/components/Home'
import LoginForm from '@/components/LoginForm'
import NavbarTop from '@/components/NavbarTop'
import Calculator from '@/components/Calculator'
import Countdown from '@/components/Countdown'
import DragAndDrop from '@/components/DragAndDrop'
import GPSMap from '@/components/GPSMap'
import APIDataFetching from '@/components/APIDataFetching'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'LoginForm',
      component: LoginForm
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/calculator',
      name: 'Calculator',
      component: Calculator
    },
    {
      path: '/countdown',
      name: 'Countdown',
      component: Countdown
    },
    {
      path: '/draganddrop',
      name: 'DragAndDrop',
      component: DragAndDrop
    },
    {
      path: '/gpsmap',
      name: 'GPSMap',
      component: GPSMap
    },
    {
      path: '/apidatafetching',
      name: 'APIDataFetching',
      component: APIDataFetching
    }
  ]
})
